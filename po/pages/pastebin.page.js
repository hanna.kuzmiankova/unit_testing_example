const OptionalPasteSettingsComponent = require("../components/common/optionalPasteSettings.component")
class PastebinLandingPage {
    constructor(){
        this.optionalSettings = new OptionalPasteSettingsComponent();
    }
async open() {
    await browser.url('https://pastebin.com');
}
}
module.exports = new PastebinLandingPage();