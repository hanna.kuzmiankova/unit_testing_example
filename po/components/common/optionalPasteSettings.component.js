class OptionalPasteSettings {
  get newPaste() {
    return $("#postform-text");
  }
  get pastExpirationSelect() {
    return $("#select2-postform-expiration-container");
  }
  get pasteNameTitle() {
    return $("#postform-name");
  }
  get createNewPaste() {
    return $("//button[text()='Create New Paste']");
  }

  getPastExpirationOption(option) {
    return $(`.select2-results__options li:nth-child(${option})`);
  }

  async choosePastExpirationOption(option) {
    await this.pastExpirationSelect.click();
    await this.getPastExpirationOption(option).click();
  }
}

module.exports = OptionalPasteSettings;
