const PastebinLandingPage = require("../../po/pages/pastebin.page");

describe("WebDriver Task 1", () => {
  const optionalPasteSettings = PastebinLandingPage.optionalSettings;
  beforeEach(async () => {
    await PastebinLandingPage.open();
  });
  it("first test", async () => {
    await optionalPasteSettings.newPaste.setValue("Hello from WebDriver");
    await optionalPasteSettings.choosePastExpirationOption(3);
    await optionalPasteSettings.pasteNameTitle.setValue("helloweb");
    await optionalPasteSettings.createNewPaste.click();
  });
});
